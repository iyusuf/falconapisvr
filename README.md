# README.md

## How to build and run
### Bare minimum requirement

- Build with Maven without running test
```
mvn clean package -Dmaven.test.skip=true
```

- Execute without using a database instance running in the background
```
java -jar target/falconapisvr-0.0.1-SNAPSHOT.jar
```

- Test with browser
```
http://localhost:9003/helloworld/
```