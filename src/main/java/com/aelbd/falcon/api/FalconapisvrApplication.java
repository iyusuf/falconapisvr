package com.aelbd.falcon.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FalconapisvrApplication {

	public static void main(String[] args) {
		SpringApplication.run(FalconapisvrApplication.class, args);
	}
}
