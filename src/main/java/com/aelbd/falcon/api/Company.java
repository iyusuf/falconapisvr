package com.aelbd.falcon.api;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name= "companies")
public class Company{

    private static final long serialVersionUID = -1L;

    @Id
    private Long id;
    //id |    name    | age |                      address

    @Column(name="name")
    private String name;

    @Column(name="age")
    private Integer age;

    @Column(name="address")
    private String address;


    //According to tutorial this is for JPA to use.
    protected Company() {
    }

    //According to tutorial this constructor is most likely for Jackson JSON to Object converter to work.
    public Company(long id, String name, Integer age, String address ){
        this.id=id;
        this.name=name;
        this.age=age;
        this.address=address;
    }

    @Override
    public String toString() {
        return "Company{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", address='" + address + '\'' +
                '}';
    }

    // All these getters and setters are probably needed by Jackson JSON to Java Object mapper to use.
    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getAge() {
        return age;
    }

    public String getAddress() {
        return address;
    }
}
