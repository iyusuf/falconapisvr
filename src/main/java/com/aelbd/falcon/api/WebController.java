package com.aelbd.falcon.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by iyusuf on 2/16/2017.
 */
@RestController
public class WebController {

    private final CompanyRepository companyRepository;

    @Autowired
    WebController(CompanyRepository companyRepository){
        this.companyRepository=companyRepository;
    }

    @RequestMapping("/helloworld")
    public String helloWorld(){
        return "Hello from Falcon API Server";
    }

    @RequestMapping("/findbyname")
    public ResponseEntity<Company> findByName(@RequestParam("name") String companyName) {
        String result = "<html>";

        for (Company c : companyRepository.findByName(companyName)) {
            result += "<div>" + c.toString() + "</div>";

            System.out.println("Found Company " + c.toString());
            return new ResponseEntity<Company>(c, HttpStatus.OK);
        }

            return new ResponseEntity<Company>(HttpStatus.NO_CONTENT);
    }
}
